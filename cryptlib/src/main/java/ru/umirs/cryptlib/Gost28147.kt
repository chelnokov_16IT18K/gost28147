package ru.umirs.cryptlib

import org.bouncycastle.crypto.CipherParameters
import org.bouncycastle.crypto.engines.GOST28147Engine
import org.bouncycastle.crypto.macs.GOST28147Mac
import org.bouncycastle.crypto.params.KeyParameter
import org.bouncycastle.crypto.params.ParametersWithSBox
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.util.encoders.Hex
import java.security.Security

/**
 * Класс, содержащий за реализацию алгоритма ГОСТ28147-89 в режиме
 * выработки имитовставки.
 *
 */
class Gost28147 {

    companion object Factory {

        /**
         * возвращает MAC в виде строки с шестнадцатеричным представлением значения имитовставки
         *
         * @param message - текст сообщения для вычисления MAC
         * @param key - значение поля ключа
         *
         * @return восьмисимвольная строка размера 256 бит(32 байта), содержащая MAC
         */
        @ExperimentalUnsignedTypes
        fun computeMac(message: String, key: String): String {
            init()

            val mac = GOST28147Mac()
            //установка параметров ключа и используемой таблицы замен
            val param: CipherParameters = ParametersWithSBox(
                KeyParameter(generateKey(key)), GOST28147Engine.getSBox(
                    "Param-Z"//в соответствии с пунктом 2
                )
            )

            mac.init(param)
            mac.update(message.toByteArray(), 0, message.toByteArray().size)

            val bytes = ByteArray(4)//32 бит - максимальный размер MAC
            mac.doFinal(bytes, 0)

            mac.reset()

            /** конечное форматирование MAC по определённому правилу
            с побитовым обходом строки, см. пункт 2 */
            var resultString = String()//строка с шестнадцатеричным представлением 8 чисел
            for (b in bytes) {
                var a = b.toInt()
                var a1: Int
                var a2: Int
                var a3: Int
                var a4: Int
                var m: Int
                for (i in 0..1) { //работа с полубайтами
                    a1 = if (a and 128 == 0) 0 else 8
                    a = a shl 1
                    a2 = if (a and 128 == 0) 0 else 4
                    a = a shl 1
                    a3 = if (a and 128 == 0) 0 else 2
                    a = a shl 1
                    a4 = if (a and 128 == 0) 0 else 1
                    a = a shl 1

                    m = a1 + a2 + a3 + a4
                    resultString += Integer.toHexString(m)
                }
            }
            return resultString
        }

        /**
         * возвращает строку, содержащую ключ в шестнадцатеричном (HEX) формате
         * @return форматированная строка с ключом
         */
        fun getHexKey(key: String): String {
            return Hex.toHexString(key.toByteArray())
        }

        private fun init() {
            Security.removeProvider("BC")
            val provider = BouncyCastleProvider()
            Security.insertProviderAt(provider, 1)
        }

        /**
         * Генерирует ключ, используя алгоритм формирования ключа на основе
         * ключевой строки в пункте 4.2
         *
         * @param key строка, заданная в поле ключа
         *
         * @return 32-битный массив байт, содержащий сгенерированный ключ
         */
        @ExperimentalUnsignedTypes
        fun generateKey(key: String): ByteArray {
            val byteKey = key.toByteArray()//перевод в байтовый массив, п. 4.2.2
            val uByteKey: UByteArray = byteKey.asUByteArray()//перевод в беззнаковый формат

            /*последний бит последовательности для реализации пункта 4.2.3*/
            val lastByte = uByteKey.last().toUInt().shl(7).toUByte()

            //массив для мультиплицирования исходной последовательности, п. 4.2.4
            var newKeyArray = UByteArray(0)
            var bitCount = 0//величина побитового сдвига

            var highPart: UByte//старшая часть байта
            var lowPart: UByte = (0b00000000).toUByte() // младшая часть байта

            var i = 0
            while (i <= uByteKey.size) {
                if (i == 0) {
                    bitCount++
                    highPart = takeHigh(uByteKey[i], bitCount)

                    //добавление единицы перед каждым повторением последовательности, п.4.2.3
                    newKeyArray += lowPart.or(lastByte.toUInt().shr(bitCount - 1).toUByte())
                        .or(highPart)
                    lowPart = takeLow(uByteKey[i], 8 - bitCount)
                    i++
                }
                //поэлементное копирование массива с побитовым сдвигом
                highPart = takeHigh(uByteKey[i], bitCount)
                newKeyArray += lowPart.or(highPart)
                lowPart = takeLow(uByteKey[i], 8 - bitCount)
                i++

                //Ситуация, при которой биты за пределами байта формируют ещё один байт
                if (bitCount == 8) {
                    newKeyArray += lowPart
                    bitCount = 0
                }

                /*Условие прекращения цикла. Длина полученного массива должна быть больше либо равна 32,
                * и исходная последовательность должна быть полностью скопирована, п. 4.2.4.*/
                if (i == uByteKey.size) {
                    if (newKeyArray.size >= 32) {
                        break
                    } else {
                        i = 0
                    }
                }
            }
            newKeyArray += lowPart//добавление оставшихся за пределами битов в виде отдельного байта

            // массив с дополнительно побитово сдвинутыми значениями для реализации пункта 4.2.5
            val finalKeyArray = UByteArray(newKeyArray.size)
            i = newKeyArray.size - 1

            lowPart = lowPart.toUInt().shr(8 - bitCount).toUByte()

            //реализация обратного сдвига, младшая часть предыдущего бита становится старшей частью следующего
            while (i > 0) {
                highPart = takeLow(newKeyArray[i - 1], bitCount)
                finalKeyArray[i] = (highPart.or(lowPart))
                lowPart = takeHigh(newKeyArray[i - 1], 8 - bitCount)
                i--
            }

            //реализация пункта 4.2.5.
            val key256digits = finalKeyArray.drop(finalKeyArray.size - 32).toUByteArray()

            return key256digits.toByteArray()
        }

        /**
         * Возвращает младшую часть байта в виде отдельного байта путём поразрядного сдвига
         *
         * @param uByte исходный байт
         * @param bitCount - величина сдвига
         */
        @ExperimentalUnsignedTypes
        private fun takeLow(uByte: UByte, bitCount: Int): UByte {
            var uInt = uByte.toUInt()
            uInt = uInt.shl(bitCount)
            return uInt.toUByte()
        }

        /**
         * Возвращает старшую часть байта в виде отдельного байта путём поразрядного сдвига
         *
         * @param uByte исходный байт
         * @param bitCount - величина сдвига
         */
        @ExperimentalUnsignedTypes
        private fun takeHigh(uByte: UByte, bitCount: Int): UByte {
            var uInt = uByte.toUInt()
            uInt = uInt.shr(bitCount)
            return uInt.toUByte()
        }

    }
}

