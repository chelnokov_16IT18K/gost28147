package ru.umirs.gost28147

import org.junit.Test

import org.junit.Assert.*
import ru.umirs.cryptlib.Gost28147

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @ExperimentalUnsignedTypes
    @Test
    fun generateKey_isCorrect(){
        val gost28147 = Gost28147
        /*val expected: Array<Int> = arrayOf(0b10100000,0b10100001,0b00100001,0b11010000,0b01010000,0b10010000,
            0b11101000,0b00101000,0b01001000,0b01110100,0b00010100,0b00100100,0b00111010,0b00001010,0b00010010,
            0b00011101,0b00000101,0b00001001,0b00001110,0b10000010,0b10000100,0b10000111,0b01000001,0b01000010,
            0b01000011,0b10100000,0b10100001,0b00100001,0b11010000,0b01010000,0b10010000,0b11101000,0b00101000,
            0b01001000,0b01100000)//последовательность, скопированная 11 раз, как в примере*/

        //массив сдвинут таким образом, чтобы от конца можно было отрезать ровно 256 бит
        val expected: Array<Int> = arrayOf(0b00000101,0b00000101,0b00001001,0b00001110,0b10000010,0b10000100,0b10000111,
            0b01000001,0b01000010,0b01000011,0b10100000,0b10100001,0b00100001,0b11010000,0b01010000,0b10010000,0b11101000,
            0b00101000,0b01001000,0b01110100,0b00010100,0b00100100,0b00111010,0b00001010,0b00010010,0b00011101,0b00000101,
            0b00001001,0b00001110,0b10000010,0b10000100,0b10000111,0b01000001,0b01000010,0b01000011)


        var expectedUByte = UByteArray(expected.size)
        val resultByte = gost28147.generateKey("ABC").toUByteArray()

        for (i in expected.indices){
            expectedUByte[i] = expected[i].toUByte()
        }
        expectedUByte = expectedUByte.drop(expectedUByte.size - 32).toUByteArray()

        assertArrayEquals(expectedUByte.toByteArray(), resultByte.toByteArray())

    }
}