package ru.umirs.gost28147

import android.graphics.Typeface
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import ru.umirs.cryptlib.Gost28147


@ExperimentalUnsignedTypes
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textHexView: TextView = findViewById(R.id.hexKeyTextView)
        val macHexView: TextView = findViewById(R.id.hexMacTextView)

        textHexView.typeface = Typeface.createFromAsset(assets, "fonts/SourceCodePro-Regular.ttf")
        macHexView.typeface = Typeface.createFromAsset(assets, "fonts/SourceCodePro-Regular.ttf")

        textHexView.text = getString(R.string.hex_default)//демо строки
        macHexView.text = getString(R.string.mac_default)

        val subscribeBtn: Button = findViewById(R.id.subscribe_btn)
        val checkBtn: Button = findViewById(R.id.check_btn)

        subscribeBtn.setOnClickListener {
            validateInput()
        }

        checkBtn.setOnClickListener {
            validateInput()
        }
    }


    private fun validateInput() {
        val keyEdit: EditText = findViewById(R.id.keyInputEdit)
        val messageNumberEdit: EditText = findViewById(R.id.messageNumberEdit)
        val messageEdit: EditText = findViewById(R.id.messageTextEdit)
        val keyHexView: TextView = findViewById(R.id.hexKeyTextView)
        val macHexView: TextView = findViewById(R.id.hexMacTextView)

        if (keyEdit.text.isEmpty()) {
            keyEdit.setError(getString(R.string.keyEditWarning), AppCompatResources.getDrawable(this, R.drawable.ic_baseline_warning_24))
        }else{
            keyEdit.error = null
        }

        if (messageNumberEdit.text.length != 5) {
            messageNumberEdit.setError(getString(R.string.messageNumberEditWarning), AppCompatResources.getDrawable(this, R.drawable.ic_baseline_warning_24))
        }else{
            messageNumberEdit.error = null
        }

        if (messageEdit.text.isEmpty()) {
            messageEdit.setError(getString(R.string.messageEditWarning), AppCompatResources.getDrawable(this, R.drawable.ic_baseline_warning_24))
        }else{
            messageEdit.error = null
        }

        if (keyEdit.text.isNotEmpty() && messageNumberEdit.text.length == 5 && messageEdit.text.isNotEmpty()){
            keyHexView.text = Gost28147.getHexKey(keyEdit.text.toString())
            macHexView.text = Gost28147.computeMac(messageEdit.text.toString(), keyEdit.text.toString())
        }
    }
}